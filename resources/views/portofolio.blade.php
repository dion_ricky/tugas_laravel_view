@extends('layouts.app')

@section('title', 'Portofolio')

@section('content')
<!-- <?php var_dump($quotes) ?> -->
<main>
<div class="album py-5 bg-light">
  <div class="container">

    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

      @foreach ($quotes as $quote)
      <div class="col">
        <div class="card shadow-sm">
          <!-- <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg> -->
          <img src="https://source.unsplash.com/random?sig={{$loop->index}}" width="100%" height="225" alt="" srcset="">
          <div class="card-body">
            <p class="card-text">{{ $quote['text'] }}</p>
            <div class="d-flex justify-content-between align-items-center">
              <small class="text-muted">{{ $quote['author'] }}</small>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

</main>
@endsection

@section('style')
    @parent

    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      img {
        object-fit: cover;
      }
@endsection

