<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/portofolio', function () {
    $response = Http::get('https://goquotes-api.herokuapp.com/api/v1/random?count=6');
    return view('portofolio', ['quotes' => $response->json()['quotes']]);
});

Route::get('/pricing', function () {
    return view('pricing');
});